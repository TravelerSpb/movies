﻿using System.Windows;
using Movies.ViewModels;

namespace Movies
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var viewModel = new MainWindowViewModel();
            var win = new MainWindow {DataContext = viewModel};
            win.Show();
        }
    }
}