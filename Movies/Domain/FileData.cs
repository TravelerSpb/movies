﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain
{
    public class FileData
    {
        public string Filename { get; set; }
        public string Fileurl { get; set; }
    }
}
