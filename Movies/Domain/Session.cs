﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain
{
    public class Session
    {
        // ReSharper disable once InconsistentNaming
        public string Id { get; set; }

        public string SessionToken { get; set; }
    }
}
