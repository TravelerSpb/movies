﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain
{
    public class UploadFileMeta
    {
        public string fileName { get; set; }
        public string originalFileName { get; set; }
    }
}
