﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Movies.Annotations;
using Newtonsoft.Json;

namespace Movies.Domain
{
    public class User: INotifyPropertyChanged
    {
        [JsonProperty("_id")]
        public string IgnoreOnSerializingSetter { set { _id = value; } }

        [JsonIgnore]
        private string _id;

        [JsonIgnore]
        public string Id
        {
            get { return this._id; }
            set { this._id = value; }
        }


        private string _email;
        [JsonProperty("username")]
        public string Email { get{return _email;} set { _email = value; OnPropertyChanged(); } }

        private string _pass;
        [JsonProperty("password")]
        public string Password { get { return _pass; } set { _pass = value; OnPropertyChanged(); } }

        private string _name;
        [JsonProperty("Name")]
        public string FullName { get { return _name; } set { _name = value; OnPropertyChanged(); } }


        private string _company;
        [JsonProperty("company")]
        public string Company { get { return _company; } set { _company = value; OnPropertyChanged(); } }


        //public string acl { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
