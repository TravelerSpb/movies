﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Movies.Domain;
using Movies.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace Movies.Infrastructure
{
    public sealed class ApperyClient
    {
        private ApperyClient()
        {
        }

        public static ApperyClient Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new ApperyClient();
                    }
                }

                return _instance;
            }
        }

        public async Task<bool> Login(LoginViewModel loginInfo)
        {
            /*
             * curl -X GET \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              -G --data-urlencode 'username=<user_name>' \
                 --data-urlencode 'password=<user_password>' \
              https://api.appery.io/rest/1/db/login
            */
            var client = CreateClient("loginUrl");

            var parameters = new NameValueCollection
            {
                {"username", loginInfo.UserName},
                {"password", loginInfo.Password}
            };
            var request = CreateRequest(Method.GET, false, parameters: parameters);
            var temp = await client.ExecuteTaskAsync<Session>(request);

            if (temp.Data == null || temp.Data.SessionToken == null)
                return false;
            Session = temp.Data;
            OnMessageChanged("Login successful");
            return true;
        }

        public void Logout()
        {
            /*
             * curl -X GET \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              -H "X-Appery-Session-Token: <session_token>" \
              https://api.appery.io/rest/1/db/logout
            */
            var client = CreateClient("logoutUrl");
            var request = CreateRequest(Method.GET, true);

            client.ExecuteAsync(request, responce => { });
        }

        public async Task GetFile()
        {
            /*
              curl -X GET \
              https://api.appery.io/rest/1/db/files/54957d79e4b0262b47c42538/<file_name>\
              [?][encoded="base64"][&][sessionToken=<session_token>]\
              [&][masterKey=<master_key>]
            */
            var parameters = new NameValueCollection
            {
                {"encoded", "base64"}
            };
            var client = CreateClient("filesUrl", true);
            var request = CreateRequest(Method.GET, true, true, parameters,
                "e9d1617a-9df6-4e8e-b628-9312bb32c789.CapturFiles-201512365_111279.png");
            var temp = await client.ExecuteTaskAsync(request);
        }

        public async Task GetImage()
        {
            /*
             * curl -X GET \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              [-H "X-Appery-Session-Token: <session_token>"] \
              https://api.appery.io/rest/1/db/collections/Images/<object_id>
             */
            var client = CreateClient("imagesUrl");
            var request = CreateRequest(Method.GET, true, resource: "54fa5aece4b09c8cfffcf844");

            var temp = await client.ExecuteTaskAsync(request);
        }

        public async Task<FileData> UploadFile(byte[] fileData, string fileName)
        {
            /*
             * curl -X POST \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              -H "X-Appery-Session-Token: <session_token>" \
              -H "Content-Type: <content_type>" \
              --data-binary '<file_content>' \
              https://api.appery.io/rest/1/db/files/<file_name>
             */
            var properFileName = fileName.ToCharArray().Where(x => !Char.IsWhiteSpace(x)); // Get rid of witesps
            fileName = String.Join("", properFileName);
            var extension = Path.GetExtension(fileName);

            var client = CreateClient("filesUrl");
            client.HttpFactory = new FactoryWithContent
            {
                GetBytes = () => new Bytes(fileData,
                    "application/" + extension.Substring(1))
            };

            var request = CreateRequest(Method.POST, true, false, null, fileName);
            request.AddHeader("Content-Type", "multipart/form-data");

            var temp = await client.ExecuteTaskAsync<FileData>(request);
            OnMessageChanged(temp.Content);
            return temp.Data;
        }

        public async Task<string> CreateImage(CreateImageViewModel imageViewModel) // ToDo Decouple VM
        {
            /*
             * curl -X POST \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              [-H "X-Appery-Session-Token: <session_token>"] \
              -H "Content-Type: application/json" \
              -d '{"<field_name>":"<field_value>"}' \
              https://api.appery.io/rest/1/db/collections/Images
             */
            var client = CreateClient("imagesUrl");
            var files = imageViewModel.FileUploadViewModels;

            var parameters = new
            {
                ImageLocation = imageViewModel.Location,
                ImageName = imageViewModel.Name,
                ReportPDF = new UploadFileMeta
                {
                    fileName = files["ReportPDF"].ServerFileName,
                    originalFileName = files["ReportPDF"].ShortLocalFileName
                },
                SummaryPNG = new UploadFileMeta
                {
                    fileName = files["SummaryPNG"].ServerFileName,
                    originalFileName = files["SummaryPNG"].ShortLocalFileName
                },
                DetailPNG = new UploadFileMeta
                {
                    fileName = files["DetailPNG"].ServerFileName,
                    originalFileName = files["DetailPNG"].ShortLocalFileName
                }
                //acl = "{\"*\": {\"write\": true, \"read\": true}}", !Defaults
            };

            var request = CreateRequest(Method.POST, true, false, null);
            request.AddJsonBody(parameters);
            request.AddHeader("Content-Type", "application/json");
            var temp = await client.ExecuteTaskAsync(request);
            OnMessageChanged(temp.Content);

            return temp.Content;
        }

        /// <summary>
        ///     Create REST client factory
        /// </summary>
        private RestClient CreateClient(string url, bool useDbName = false)
        {
            var urlString = useDbName
                ? new Uri(new Uri(_appSettings[url]), _appSettings["databaseId"])
                : new Uri(_appSettings[url]);
            var restClient = new RestClient(urlString);
            return restClient;
        }

        /// <summary>
        ///     New REST request factory
        /// </summary>
        private RestRequest CreateRequest(Method method, bool isSession, bool useMasterKey = false,
            NameValueCollection parameters = null, string resource = null)
        {
            var request = new RestRequest(method);
            request.AddHeader("X-Appery-Database-Id", _appSettings["databaseId"]);

            if (resource != null)
                request.Resource = resource;

            if (parameters != null)
            {
                foreach (var parameter in parameters.AllKeys)
                    request.AddParameter(parameter, parameters[parameter]);
            }

            if (useMasterKey)
                request.AddParameter("masterKey", _appSettings["masterKey"]);

            if (isSession)
                request.AddHeader("X-Appery-Session-Token", Session.SessionToken);

            request.JsonSerializer = new JsonSerializer();
            return request;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            /*
             curl -X GET \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              -H "X-Appery-Session-Token: <session_token>" \
              https://api.appery.io/rest/1/db/users/<user_id>
             */
            var client = CreateClient("usersUrl");
            var request = CreateRequest(Method.GET, true);

            var temp = await client.ExecuteTaskAsync(request);
            return JsonConvert.DeserializeObject<User[]>(temp.Content);
        }

        public async Task<User> UpdateUser(User user)
        {
            /*
             curl -X PUT \
               -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
               -H "X-Appery-Session-Token: <session_token>" \
               -H "Content-Type: application/json" \
               -d "{\"username\":\"<user_name>\",\"password\":\"<user_password>\"}" \
               https://api.appery.io/rest/1/db/users/<user_id>
             */
            var client = CreateClient("usersUrl");

            var request = CreateRequest(Method.PUT, true, resource: user.Id);
            request.AddJsonBody(user);
            request.AddHeader("Content-Type", "application/json");

            var temp = await client.ExecuteTaskAsync(request);
            OnMessageChanged(temp.Content);

            return JsonConvert.DeserializeObject<User>(temp.Content);
        }

        public async Task<User> CreateUser(User user)
        {
            /*
             curl -X POST \
              -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
              -H "Content-Type: application/json" \
              -d "{\"username\":\"<user_name>\",\"password\":\"<user_password>\"}" \
              https://api.appery.io/rest/1/db/users
             */
            var client = CreateClient("usersUrl");
            var request = CreateRequest(Method.POST, true);

            request.AddJsonBody(user);
            request.AddHeader("Content-Type", "application/json");

            var temp = await client.ExecuteTaskAsync(request);
            OnMessageChanged(temp.Content);

            return JsonConvert.DeserializeObject<User>(temp.Content);
        }

        public async Task DeleteUser(User deletedUser)
        {
            /*
             * curl -X DELETE \
                  -H "X-Appery-Database-Id: 54957d79e4b0262b47c42538" \
                  -H "X-Appery-Session-Token: <session_token>" \
                  https://api.appery.io/rest/1/db/users/<user_id>
             */
            var client = CreateClient("usersUrl");
            var request = CreateRequest(Method.DELETE, true, resource: deletedUser.Id);
            
            var temp = await client.ExecuteTaskAsync(request);
            OnMessageChanged(temp.Content);
        }

        #region instanse vars

        private Session Session { get; set; }
        private readonly NameValueCollection _appSettings = ConfigurationManager.AppSettings;
        private static volatile ApperyClient _instance;
        private static readonly object SyncRoot = new Object();
        public event EventHandler<string> MessageChanged;

        public void OnMessageChanged(string message)
        {
            var handler = MessageChanged;
            if (handler != null) handler(this, message);
        }

        #endregion
    }
}