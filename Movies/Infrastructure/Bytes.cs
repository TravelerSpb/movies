namespace Movies.Infrastructure
{
    public class Bytes
    {
        public Bytes(byte[] value, string type)
        {
            Value = value;
            Type = type;
        }

        public byte[] Value { get; private set; }
        public string Type { get; private set; }
    }
}