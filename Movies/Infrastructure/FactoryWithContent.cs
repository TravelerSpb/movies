using System;
using RestSharp;

namespace Movies.Infrastructure
{
    public class FactoryWithContent : IHttpFactory
    {
        public IHttp Create()
        {
            var http = new Http();

            var getBytes = GetBytes;
            if (getBytes != null)
            {
                var bs = getBytes();
                http.RequestBodyBytes = bs.Value;
                http.RequestContentType = bs.Type;
            }

            return http;
        }

        public Func<Bytes> GetBytes { get; set; }
    }
}