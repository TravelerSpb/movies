﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Movies.Annotations;

namespace Movies.ViewModels
{
    public class CreateImageViewModel : INotifyPropertyChanged
    {
        private int _location;
        private string _name;

        public CreateImageViewModel()
        {
            FileUploadViewModels = new Dictionary<string, FileUploadViewModel>();
        }

        public void AddNewFile(string file, FileUploadViewModel newFile)
        {
            if (FileUploadViewModels.ContainsKey(file))
                FileUploadViewModels[file] = newFile;
            else
                FileUploadViewModels.Add(file, newFile);

            OnPropertyChanged("IsReadyToUpload");
        }

        /// <summary>
        ///     Files been uploaded
        /// </summary>
        public Dictionary<string, FileUploadViewModel> FileUploadViewModels { get; set; }

        public int Location
        {
            get { return _location; }
            set
            {
                _location = value;
                OnPropertyChanged("IsReadyToUpload");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("IsReadyToUpload");
            }
        }

        public bool IsReadyToUpload
        {
            get
            {
                return !String.IsNullOrEmpty(Name) &&
                       Location != 0 &&
                       FileUploadViewModels.Keys.Count == 3;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}