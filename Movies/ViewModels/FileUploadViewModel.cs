﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.ViewModels
{
    public class FileUploadViewModel
    {
        /// <summary>
        /// Server generated from response
        /// </summary>
        public string ServerFileName { get; set; }

        /// <summary>
        /// Local file
        /// </summary>
        public string LocalFileName { get; set; }

        /// <summary>
        /// Get file name wothout a path
        /// </summary>
        public string ShortLocalFileName
        {
            get { return Path.GetFileName(LocalFileName); }
        }

    }
}
