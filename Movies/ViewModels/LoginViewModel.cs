﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Security;
using System.Windows.Input;
using Movies.Annotations;
using Movies.Infrastructure;
using Movies.Views;

namespace Movies.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public LoginViewModel()
        {
            IsNotLogged = true;
        }

        private RelayCommand _login;
        public EventHandler<bool> LoginExecuted;

        private bool _isNotLogged;
        public bool IsNotLogged
        {
            get { return _isNotLogged; }
            set { _isNotLogged = value; OnPropertyChanged("IsNotLogged"); }
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        public ICommand LoginCommand
        {
            get
            {
                if (_login == null)
                    _login = new RelayCommand(param => OnLogin(null, null));

                return _login;
            }
        }

        private async void OnLogin(object sender, EventArgs e)
        {
            var isLogon = await ApperyClient.Instance.Login(this);
            
            this.IsNotLogged = false;
            
            var handler = LoginExecuted;
            if (handler != null)
                handler(this, isLogon);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}