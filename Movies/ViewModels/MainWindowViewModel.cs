﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using Movies.Infrastructure;
using Movies.Views;
using System.IO;
using System.Runtime.CompilerServices;
using Movies.Annotations;

namespace Movies.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            CreateImageViewModel = new CreateImageViewModel()
            {
                Name = "New Image",
                Location = 0,
            };
            LoginViewModel = new LoginViewModel();
            UserManagementViewModel = new UserMgmtViewModel();
            ApperyClient.Instance.MessageChanged += InstanceOnMessageChanged;
        }

        private void InstanceOnMessageChanged(object sender, string message)
        {
            this.UploadStatus = message;
        }

        private RelayCommand _openLogin;
        private LoginView loginView;

        private string Token { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        public CreateImageViewModel CreateImageViewModel { get; set; }
        public UserMgmtViewModel UserManagementViewModel { get; set; }

        private string _uploadStatus;
        public string UploadStatus {
            get { return _uploadStatus; } 
            set
            {
                _uploadStatus = value; OnPropertyChanged("UploadStatus");
            }
        }

        #region Commands
        public ICommand GetReportPDF
        {
            get { return new RelayCommand(param => OnChooseFile("ReportPDF", null)); }
        }
        public ICommand GetSummaryPNG
        {
            get { return new RelayCommand(param => OnChooseFile("SummaryPNG", null)); }
        }
        public ICommand GetDetailPNG
        {
            get { return new RelayCommand(param => OnChooseFile("DetailPNG", null)); }
        }
        public ICommand CreateImageCommand
        {
            get { return new RelayCommand(param => CreateImage()); }
        }
        #endregion

        private void OnChooseFile(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != true) return;

            var fileUploadViewModel = new FileUploadViewModel
            {
                LocalFileName = openFileDialog.FileName,
            };
            CreateImageViewModel.AddNewFile((string)sender,
                fileUploadViewModel);
        }

        private async void CreateImage() // ToDo => to FileUploadVM
        {
            UploadStatus = "Creating report...";
            foreach (var file in CreateImageViewModel.FileUploadViewModels.Values)
            {
                // Get FileStream
                if(!File.Exists(file.LocalFileName))
                    throw new FileNotFoundException();

                var fileData = File.ReadAllBytes(file.LocalFileName);
                var fileName = Path.GetFileName(file.LocalFileName);

                //Send Request
                var uploadedFileData = await ApperyClient.Instance.UploadFile(fileData, fileName);

                //Get id from response & save it
                file.ServerFileName = uploadedFileData.Filename;
                UploadStatus = "File uploaded: " + file;
            }

            // Files uploaded, now lets create report
            var result = await ApperyClient.Instance.CreateImage(this.CreateImageViewModel);
            UploadStatus = "Report created. " + result;
        }

        private void OnStatusChanged()
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}