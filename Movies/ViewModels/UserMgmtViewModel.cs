﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Movies.Annotations;
using Movies.Domain;
using Movies.Infrastructure;

namespace Movies.ViewModels
{
    public class UserMgmtViewModel : INotifyPropertyChanged
    {
        private User _selectedItem;

        public UserMgmtViewModel()
        {
            Users = new ObservableCollection<User>();
            Users.CollectionChanged += UsersOnCollectionChanged;
            DirtyUsers = new HashSet<string>();
        }

        private HashSet<string> DirtyUsers { get; set; }

        public User SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public ObservableCollection<User> Users { get; set; }

        public ICommand GetUsersCommand
        {
            get { return new RelayCommand(param => GetUsers()); }
        }

        public ICommand UploadUsersCommand
        {
            get { return new RelayCommand(param => UploadUsers()); }
        }

        public ICommand DeleteUserCommand
        {
            get { return new RelayCommand(param => DeleteUser()); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void UsersOnCollectionChanged(object sender,
            NotifyCollectionChangedEventArgs args)
        {
            if (args.OldItems != null)
                foreach (User oldItem in args.OldItems)
                    oldItem.PropertyChanged -= User_PropertyChanged;

            if (args.NewItems != null)
                foreach (User newItem in args.NewItems)
                    newItem.PropertyChanged += User_PropertyChanged;
        }

        private void User_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            DirtyUsers.Add(((User) sender).Email);
        }

        private async void DeleteUser()
        {
            var item = SelectedItem;
            if (SelectedItem != null && SelectedItem.Id != null) // can be created but not uploaded
            {
                await ApperyClient.Instance.DeleteUser(item);
                await GetUsers();
            }
            Users.Remove(SelectedItem);
        }

        private async Task GetUsers()
        {
            var collection = await ApperyClient.Instance.GetUsers();
            Users.Clear();

            foreach (var user in collection)
                Users.Add(user);
        }

        private async void UploadUsers()
        {
            var usersToUpdate = Users.Where(x => DirtyUsers.Contains(x.Email)).ToArray();

            for (var i = 0; i < usersToUpdate.Count(); i++)
            {
                var user = usersToUpdate[i];

                if (String.IsNullOrEmpty(user.Id)) // New user
                    await ApperyClient.Instance.CreateUser(user);
                else // update
                    await ApperyClient.Instance.UpdateUser(user);

                await GetUsers();
            }
            DirtyUsers.Clear();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}